#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/uaccess.h>



MODULE_LICENSE (" GPL ");
MODULE_AUTHOR (" Nahuel Sauma ");
MODULE_DESCRIPTION ("Un primer driver ");

int init_module(void);
void cleanup_module(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

#define SUCCESS 0
#define DEVICE_NAME "miModulo" 
#define PROCFS_MAX_SIZE 2048 

/*variables*/
static int Major; /* Major number assigned to our device driver */
static int Device_Open = 0; /* Is device open? Used to prevent multiple access to device */


static unsigned long procfs_buffer_size = 0;
static char procfs_buffer[PROCFS_MAX_SIZE];



static struct file_operations fops={
    .read   = device_read,
    .write  = device_write,
    .open   = device_open,
    .release    = device_release
};

/* Funcion de inicio */
int init_module(void){
    Major = register_chrdev(0, DEVICE_NAME, &fops);
    if (Major < 0) {
        printk(KERN_ALERT "Registering char device failed with %d\n", Major);
        return Major;
        }
        printk(KERN_INFO "I was assigned major number %d. To talk to\n", Major);
        printk(KERN_INFO "the driver, create a dev file with\n");
        printk(KERN_INFO "'mknod /dev/%s c %d 0'.\n", DEVICE_NAME, Major);
        printk(KERN_INFO "Try various minor numbers. Try to cat and echo to\n");
        printk(KERN_INFO "the device file.\n");
        printk(KERN_INFO "Remove the device file and module when done.\n");

        return SUCCESS;
}

/* Funcion de fin */
void cleanup_module(void){
    unregister_chrdev(Major, DEVICE_NAME);
}

/* Metodos */

/* device_open. Se ejecuta cuando un proceso trata de abrir el device file (cat miModulo) */
static int device_open(struct inode *inode, struct file *file){
    if (Device_Open){
        return -EBUSY;
    }

    Device_Open++;

    try_module_get(THIS_MODULE);
    return SUCCESS;
}

/* device_release. Se ejecuta cuando un proceso cierra el device file */
static int device_release(struct inode *inode, struct file *file){
    Device_Open--; 

   module_put(THIS_MODULE);
   return 0;
}

/*
 * Called when a process, which already opened the dev file, attempts to
 * read from it.
 */
static ssize_t device_read( struct file *filp, /* see include/linux/fs.h */
                            char *buffer, /* buffer to fill with data */
                            size_t length, /* length of the buffer */
                            loff_t * offset){
    
    static int finished = 0;

    if(finished) {
        printk(KERN_INFO "procfs_read: END\n");
        finished =0;
        return 0;
    }

    finished =1;
    if(copy_to_user(buffer, procfs_buffer, procfs_buffer_size)){
        return -EFAULT;
    }

    printk(KERN_INFO "procfs_read: read %lu bytes\n", procfs_buffer_size);
    return procfs_buffer_size;
    
}

/*  
 *Se ejecuta cuando un proceso escribe al device. Ej echo "hola" > miModulo
 */
static ssize_t device_write(struct file *filp,
                            const char *buff,
                            size_t len,
                            loff_t * off){

    if(len > PROCFS_MAX_SIZE) {
        procfs_buffer_size = PROCFS_MAX_SIZE;
    }
    else{
        procfs_buffer_size = len;
    }

    //limpiar el buffer
    memset (procfs_buffer, 0, sizeof procfs_buffer);

    if(copy_from_user(procfs_buffer, buff, procfs_buffer_size)) {
        return -EFAULT;
    }

    printk(KERN_ALERT "Input exitoso: %s \n", procfs_buffer);
    return procfs_buffer_size;

    //copy_from_user(   Direccion de destino en el kernel space,
    //                  Direccion de origen en el user space,
    //                  Numero de bytes a copiar)
    //return    numero de bytes que no pudo copiar, en exito es 0.
}

                         







































































































