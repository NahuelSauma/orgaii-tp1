OrgaII TP1 "Drivers"

Proceso:

1- Usando el código provisto se crearon los archivos makeFile y miModulo.c

2- Se ejecuto el comando make para compilar el archivo y crear el archivo.ko como se muestra en la imagen (1)

3- Se realizó un modinfo como se muestra en la imagen (2) para ver los datos

4- Se ejecuto el comando "sudo insmod ./miModulo.ko" para instalar el módulo

5- Se navegó a la carpeta "/proc/modules" para verificar que el módulo se haya instalado como se muestra en la imagen (3)

6- Se ejecuto el comando "dmesg" para leer el log del kernel y verificar la instalación/ desinstalación del módulo, imagen (4)

7- Se modifica el archivo "miModulo.c" siguiendo los ejemplos del capítulo 4.1.5 de "The Linux Kernel Module Programming Guide" para crear
    un character device que imprima por pantalla la cantidad de veces que se lo abrió.

8- Se instaló el nuevo módulo y se imprimió por el log del kernel la información necesaria para crear el device file en la carpeta /dev como muestra
    la imagen (5).

9- Se ejecuto el comando cat para comprobar que el módulo imprima por consola el mensaje correspondiente como muestra la imagen (6).

10- Se modificó el archivo "miModulo.c" siguiendo los ejemplos del capítulo 5.3 de "The Linux Kernel Module Programming Guide" para modificar la
    funcionalidad y que ahora el device_write y device_read utilicen las funciones copy_to_user y copy_from_user. Estos métodos son utilizados para
    tomar los mensajes desde el user space y guardarlos en un buffer en el kernel space, una vez allí se utiliza un printk para imprimir en el log de
    kernel el mensaje recibido. Luego al realizar un "cat" el copy_to_user vuelve a traer el mensaje desde el buffer en kernel space al user space para
    que pueda ser mostrado por pantalla como muestra la imagen (7).

